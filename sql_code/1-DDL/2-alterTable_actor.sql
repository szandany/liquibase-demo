-- liquibase formatted sql
--changeset MikeO:45556-altertableactor
ALTER TABLE actor
  ADD COLUMN twitter VARCHAR(15);
--rollback alter table actor drop column twitter;