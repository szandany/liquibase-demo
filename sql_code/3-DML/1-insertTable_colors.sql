-- liquibase formatted sql
--changeset BenG:45679-insertcolors
INSERT INTO colors (bcolor, fcolor)
VALUES
  ('red', 'red'),
  ('red', 'red'),
  ('red', NULL),
  (NULL, 'red'),
  ('red', 'green'),
  ('red', 'blue'),
  ('green', 'red'),
  ('green', 'blue'),
  ('green', 'green'),
  ('blue', 'red'),
  ('blue', 'green'),
  ('blue', 'blue');
--rollback delete from colors where bcolor = 'red' and fcolor = 'red';
--rollback delete from colors where bcolor = 'red' and fcolor IS NULL;
--rollback delete from colors where bcolor IS NULL and fcolor = 'red';
--rollback delete from colors where bcolor = 'red' and fcolor = 'green';
--rollback delete from colors where bcolor = 'red' and fcolor = 'blue';
--rollback delete from colors where bcolor = 'green' and fcolor = 'red';
--rollback delete from colors where bcolor = 'green' and fcolor = 'blue';
--rollback delete from colors where bcolor = 'green' and fcolor = 'green';
--rollback delete from colors where bcolor = 'blue' and fcolor = 'red';
--rollback delete from colors where bcolor = 'blue' and fcolor = 'green';
--rollback delete from colors where bcolor = 'blue' and fcolor = 'blue';

